from uuid import uuid4

from flask import Blueprint, jsonify, request
from spectree import Response

from toobibfree.schema.fhir.r4.datatype.code import AddressUseEnum
from toobibfree.schema.fhir.r4.datatype.datatypes import Address, Identifier
from toobibfree.schema.fhir.r4.resource.organization import (
    Organization,
    OrganizationFilter,
)
from toobibfree.schema.spec import api

organization = Blueprint("Organization", __name__)


@organization.route("/api/Organization/<uuid:id>", methods=["GET"])
@api.validate(resp=Response(HTTP_200=Organization), tags=["Ressource"])
def get_organization(id):
    """
    Get organization by id
    """

    organization = Organization(
        id=uuid4(),
        identifier=[
            Identifier(
                id=uuid4(),
                system="https://interhop.org/identifiers",
                value=str(uuid4()),
            )
        ],
        name="hopital x",
        partOf=uuid4(),
        address=[Address(city="", postalCode="", id=uuid4(), use=AddressUseEnum.home)],
    )

    return jsonify(organization.dict())


@organization.route("/api/Organization", methods=["POST"])
@api.validate(json=Organization, resp=Response(HTTP_200=Organization), tags=["Ressource"])
def post_organization():
    """
    Post a resource.. and get it back !
    """
    organization_in = request.context.json
    return jsonify(organization_in.dict())


@organization.route("/api/Organization", methods=["GET"])
@api.validate(
    query=OrganizationFilter, resp=Response(HTTP_200=Organization), tags=["Ressource"]
)
def filter_organization():
    """
    Filter organziation
    """

    filter: OrganizationFilter = request.context.query
    address = filter.address if filter.address else ""
    if address.find("paris"):
        name = "cabinet X"
    else:
        name = "cabinet Y"

    organization = Organization(
        id=uuid4(),
        identifier=[
            Identifier(
                id=uuid4(),
                system="https://interhop.org/identifiers",
                value=str(uuid4()),
            )
        ],
        name=name,
        partOf=uuid4(),
        address=[Address(city="", postalCode="", id=uuid4(), use=AddressUseEnum.home)],
    )
    return jsonify(organization.dict())
