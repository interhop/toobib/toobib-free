from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel, Field

from toobibfree.schema.fhir.r4.datatype.datatypes import Address, Identifier
from toobibfree.schema.fhir.r4.resource.domain_resource import Resource


class OrganizationFilter(BaseModel):
    active: Optional[str] = Field(
        None, description="Is the Organization record active	Organization.active"
    )
    address: Optional[str] = Field(
        None,
        description="""A server defined search that may match any of 
    the string fields in the Address, including line, city, district, 
    state, country, postalCode, and/or text	Organization.address""",
    )


class Organization(Resource):
    identifier: List[Identifier] = Field(..., description="The organization identifier")
    name: str = Field(...)
    address: List[Address] = Field(...)
    partOf: UUID = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                "identifier": [
                    {
                        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                        "system": "http://interhop.org/identifiers",
                        "value": "xxb2-ha",
                    }
                ],
                "address": [
                    {
                        "city": "paris",
                        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                        "postalCode": "75000",
                        "use": "home",
                    }
                ],
                "name": "Hospital X",
                "partOf": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            }
        }
