import pytest
import sqlalchemy
from sqlalchemy.orm import scoped_session, sessionmaker
from testcontainers.postgres import PostgresContainer

from toobibfree import app


@pytest.fixture
def db_session():
    with PostgresContainer("postgres:12.5") as postgres:
        engine = sqlalchemy.create_engine(postgres.get_connection_url(), pool_size=5)
        session = scoped_session(
            sessionmaker(autocommit=False, autoflush=True, bind=engine)
        )
        with session() as db_session:
            yield db_session
        db_session.close()


@pytest.fixture
def client():
    app.app.config["TESTING"] = True
    with app.app.test_client() as client:
        yield client
