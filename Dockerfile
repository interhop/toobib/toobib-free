FROM tiangolo/meinheld-gunicorn:python3.8

COPY ./ /app
RUN cd /app && pip install --upgrade pip
RUN cd /app && pip install -r /app/requirements.txt
RUN cd /app && python setup.py install
RUN pip install greenlet==0.4.17
