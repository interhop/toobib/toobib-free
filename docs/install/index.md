# Install with docker-compose

We provide a [high
performance](https://github.com/tiangolo/meinheld-gunicorn-docker)
docker image, ready for production.

```bash
docker-compose up -d
# check http://localhost/apidoc/swagger
```
