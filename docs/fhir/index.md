# FHIR ressources

Those can be auto-generated from the factories classes.

## Organization

```json
{
  "address": [
    {
      "city": "paris",
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "postalCode": "75000",
      "use": "home"
    }
  ],
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "identifier": [
    {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "system": "http://interhop.org/identifiers",
      "value": "xxb2-ha"
    }
  ],
  "name": "Hospital X",
  "partOf": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
}
```
