# Toobib-free


[GIT REPOSITORY](https://framagit.org/interhop/toobib/toobib-free)

# API Specs

[The openapi specs](https://framagit.org/interhop/toobib/toobib-free/-/blob/master/openapi/openapi.json)

# Project

```
├── LICENSE 
├── README.md
├── Makefile          # make invokations
├── docs              # markdown project documentation
├── mkdocs.yml        # documentation configuration
├── pyproject.toml    # project metadata
├── .gitlab-ci.yml    # ci-cd 
├── requirements.txt  # dependencies
├── tests
│   ├── conftest.py   # pytest fixture
│   └── __init__.py   # needed for pytest to work
├── toobibfree
│   ├── api           # api endpoints
│   ├── app.py        # flask application
│   ├── core          # core tools
│   ├── model         # sqlalchemy classes
│   ├── schema        # pydantic classes
│   └── transform     # object mapping (pydantic <-> sqlalchemy)
└── tox.ini           # tox configuration
```
# Build tools

```
(.venv) toobibfree$ make
help:            Show this help.
coverage:   Run tests with coverage
deps:   Install dependencies
lint:   Lint and static-check
push:   Push code with tags
test:   Run tests
```

